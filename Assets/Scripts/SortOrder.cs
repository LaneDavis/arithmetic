﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortOrder {

    // ENEMIES - 0 - 899
    public static int EnemiesMin = 500;
    public static int EnemiesMax = 899;
    
    // PLAYERS - 900 - 999
    public static int Player = 900;
    
    // BULLETS - 1000 - 1999
    public static int BulletsMin = 1000;
    public static int BulletsMax = 1999;
    
    // WALLS - 2000
    public static int Walls = 2000;

}
