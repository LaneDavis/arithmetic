﻿using UnityEngine;

public interface IPoolableObject {

    GameObject GameObject();
    void ResetForPool();
    void ReleaseToPool();

}
