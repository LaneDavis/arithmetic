﻿using UnityEngine;

public class EnemyStateFollowPlayer : EnemyState {

    public float PreferredDistance = 5f;
    public AnimationCurve SpeedByDistance = new AnimationCurve (new Keyframe(0f, 0f, 0f, 0f, 0.5f, 0.5f), new Keyframe(1f, 1f, 0f, 0f, 1f, 1f));

    private Transform t;

    public override void Initialize(EntityController entityController) {
        base.Initialize(entityController);
        t = transform;
    }
    
    public override void UpdateState() {
        base.UpdateState();
        Vector2 myPos = t.position;
        PlayerData nearestPlayer = PlayerManager.Instance.NearestPlayer(myPos);
        if (nearestPlayer == null) {
            return;
        }
        Vector2 nearestPlayerPos = nearestPlayer.PlayerObject.transform.position;
        Vector2 preferredPos = nearestPlayerPos + (myPos - nearestPlayerPos).normalized * PreferredDistance;
        Vector2 diff = preferredPos - myPos;
        Vector2 dir = diff.normalized;
        float mag = diff.magnitude;
        EntityController.Move(dir * SpeedByDistance.Evaluate(mag));
    }

}
