﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ParticleSplat : MonoBehaviour, IPoolableObject {

    private List<ParticleSystem> _particleSystems;
    private float _duration;
    private float _timeOfDeath;
    
    private void OnEnable() {
        _particleSystems = GetComponentsInChildren<ParticleSystem>().ToList();
        if (_duration <= 0f) {
            _duration = _particleSystems.Max(e => e.main.startLifetime.constantMax);
        }
        _timeOfDeath = Time.time + _duration;
        foreach (ParticleSystem ps in _particleSystems) {
            ps.Play();
        }
    }

    private void Update() {
        if (Time.time > _timeOfDeath) {
            ResetForPool();
            ReleaseToPool();
        }
    }
    
    public GameObject GameObject() {
        return gameObject;
    }

    public void ResetForPool() {
        foreach (ParticleSystem ps in _particleSystems) {
            ps.Stop();
        }
    }

    public void ReleaseToPool() {
        PoolManager.Instance.ReturnCopy(this);
    }
}
