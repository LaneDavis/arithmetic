﻿using System;
using UnityEngine;

[Serializable]
public class BuffStat : Buff {

    public float Value;
    public AnimationCurve CurveIn;
    public AnimationCurve CurveOut;

    private float _startTime;
    private float _fadeStartTime;
    
    public override bool Expired => _ending && Time.time - _fadeStartTime >= CurveOut.Duration();
    public override Positivity Valence { get {
            float v = GetValue();
            return v > 0 ? Positivity.Positive : v < 0 ? Positivity.Negative : Positivity.Neutral; }
    }

    public BuffStat (BuffStat other) {
        _startTime = Time.time;
        Value = other.Value;
        CurveIn = other.CurveIn.Copy();
        CurveOut = other.CurveOut.Copy();
    }
    
    public override EntityStatState ApplyBuff(EntityStatState prevState) {
        float v = GetValue();
        if (v >= 0) {
            prevState.MoveSpeed += v;    // Positive buffs are applied additively.
        }
        else {
            prevState.MoveSpeed *= (1f - v);    // Negative buffs are applied multiplicatively.
        }
        return prevState;
    }

    private float GetValue() {
        float v = Value * CurveIn.Evaluate(Time.time - _startTime);
        if (_ending) v *= CurveOut.Evaluate(Time.time - _fadeStartTime);
        return v;
    }
    
    public override void End() {
        base.End();
        _fadeStartTime = Time.time;
    }

}
