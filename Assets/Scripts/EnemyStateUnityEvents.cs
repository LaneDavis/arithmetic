﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyStateUnityEvents : EnemyState {

    public float WaitTime = 1f;

    public List<UnityEvent> StartEvents;
    public List<UnityEvent> UpdateEvents;
    public List<UnityEvent> EndEvents;
    
    public override void StartState () {
        base.StartState();
        StartEvents.ForEach(e => e.Invoke());
    }

    public override void UpdateState() {
        base.UpdateState();
        UpdateEvents.ForEach(e => e.Invoke());
        if (_activeTime > WaitTime) {
            EndState();
        }
    }

    public override void EndState() {
        base.EndState();
        EndEvents.ForEach(e => e.Invoke());
    }

}
