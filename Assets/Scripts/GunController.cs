﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour {

    private Transform _transform;
    
    public float FireRate;
    public GameObject BulletPrefab;
    public GameObject TrailPrefab;
    public List<FXController> FireFX;

    private float _timeOfNextShot;

    private void Start() {
        _transform = transform;
    }

    public void Shoot(Vector2 vec) {
        if (Time.time > _timeOfNextShot) {
            _timeOfNextShot = Time.time + 1f / FireRate;
            Fire(vec);
        }
    }

    private void Fire(Vector2 vec) {
        GameObject bulletObject = PoolManager.Instance.GetCopy(BulletPrefab);
        bulletObject.transform.position = _transform.position;
        bulletObject.transform.rotation = Quaternion.identity;
        bulletObject.transform.Rotate(0f, 0f, MathUtilities.RotationToLookAtVector(Vector2.right, vec));
        bulletObject.SetActive(true);
        foreach (var fxController in FireFX) {
            fxController.BuildCustom().WithInputVector(vec).AndTrigger();
        }

        if (TrailPrefab != null) {
            SetBulletTrail(bulletObject.GetComponent<BulletController>(), bulletObject.transform);
        }
    }

    private void SetBulletTrail(BulletController bulletController, Transform bulletTransform) {
        GameObject trailObject = PoolManager.Instance.GetCopy(TrailPrefab);
        trailObject.transform.position = bulletTransform.position;
        trailObject.transform.rotation = Quaternion.identity;
        trailObject.transform.Rotate(0f, 0f, MathUtilities.RotationToLookAtVector(Vector2.right, bulletTransform.right));
        BulletTrail trail = trailObject.GetComponent<BulletTrail>();
        trailObject.SetActive(true);
        trail.Init(bulletController);
    }
    
}
