﻿using UnityEngine;

public class CameraController : MonoBehaviour {

    public static CameraController Instance;
    public FXController ShakeFX;

    private void Awake() {
        Instance = this;
    }

    public void DoShake(float amplitude) {
        ShakeFX.BuildCustom().WithAmplitude(amplitude).AndTrigger();
    }

}
