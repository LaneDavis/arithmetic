﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PoolManager : MonoBehaviour {

    public static PoolManager Instance;
    private Dictionary<string, Transform> pools = new Dictionary<string, Transform>();

    private void Awake() {
        Instance = this;
    }

    /// <summary>
    /// Grab a copy of an object from a GameObject pool. If none exists, one will be instantiated.
    /// </summary>
    public GameObject GetCopy (GameObject prefabObject) {
        if (pools.ContainsKey(prefabObject.name)) {
            Transform poolParent = pools[prefabObject.name];
            if (poolParent.childCount > 0) {
                Transform returned = poolParent.GetChild(0);
                returned.SetParent(null);
                return returned.gameObject;
            }
        }
        else {
            string poolKey = GetPoolKeyFromObject(prefabObject);
            GameObject newPoolParent = Instantiate(new GameObject(), transform);
            newPoolParent.name = $"Pool — {poolKey}";
            pools.Add(poolKey, newPoolParent.transform);
        }
        return Instantiate(prefabObject, null);
    }

    /// <summary>
    /// Add an object to its GameObject pool. Make sure to reset the object first.
    /// </summary>
    public void ReturnCopy(IPoolableObject poolableObject) {
        GameObject objectInstance = poolableObject.GameObject();
        string poolKey = GetPoolKeyFromObject(objectInstance);
        if (pools.ContainsKey(poolKey)) {
            Transform poolParent = pools[poolKey];
            objectInstance.transform.SetParent(poolParent);
        }
        else {
            Debug.LogWarning($"Object {poolableObject.GameObject().name} returned without pool to go into.");
        }
        objectInstance.SetActive(false);
    }

    private string GetPoolKeyFromObject (GameObject obj) {
        string objName = obj.name;
        string nonCloneObjectName = objName.Contains('(') ? objName.Substring(0, objName.IndexOf('(')) : objName;
        foreach (var pool in pools.Where(pool => nonCloneObjectName.Equals(pool.Key))) {
            return pool.Key;
        }
        return nonCloneObjectName;
    }

}
