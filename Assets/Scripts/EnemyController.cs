﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EntityController))]
public class EnemyController : MonoBehaviour {

    protected EntityController _entityController;

    protected virtual void Awake() {
        _entityController = GetComponent<EntityController>();
    }

    protected void CreateStateLoop(List<EnemyState> states) {
        for (int i = 0; i < states.Count; i++) {
            if (i == states.Count - 1) {
                states[i].RegisterForStateEnd(state => states[0].StartState());
            }
            else {
                var i1 = i;
                states[i].RegisterForStateEnd(state => states[i1 + 1].StartState());
            }
        }
    }

    protected virtual void OnStateStarted(EnemyState state) {
        
    }

}
