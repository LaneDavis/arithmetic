﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemyState : MonoBehaviour {

    protected EntityController EntityController;

    private readonly List<Action<EnemyState>> _stateStartedActions = new List<Action<EnemyState>>();
    private readonly List<Action<EnemyState>> _stateEndedActions = new List<Action<EnemyState>>();

    protected bool _isActive;
    protected float _activeTime;
    
    public virtual void Initialize(EntityController entityController) {
        EntityController = entityController;
    }
    
    public virtual void StartState () {
        foreach (Action<EnemyState> action in _stateStartedActions) {
            action.Invoke(this);
        }
        _isActive = true;
    }

    public virtual void UpdateState() {
        _activeTime += Time.deltaTime;
    }

    public virtual void EndState() {
        foreach (Action<EnemyState> action in _stateEndedActions) {
            action.Invoke(this);
        }
        _activeTime = 0f;
        _isActive = false;
    }

    public void RegisterForStateStart(Action<EnemyState> action) {
        _stateStartedActions.AddIfUnique(action);
    }

    public void UnregisterForStateStart(Action<EnemyState> action) {
        _stateStartedActions.Remove(action);
    }
    
    public void RegisterForStateEnd(Action<EnemyState> action) {
        _stateEndedActions.AddIfUnique(action);
    }

    public void UnregisterForStateEnd(Action<EnemyState> action) {
        _stateEndedActions.Remove(action);
    }
    
}
