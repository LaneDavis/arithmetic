﻿using System.Collections.Generic;
using System.Linq;
using Rewired;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager Instance;
    
    private readonly List<PlayerData> _playerDatas = new List<PlayerData>();

    private void Awake() {
        Instance = this;
    }
    
    public void RegisterPlayerEntity (EntityController newPlayerController) {
        if (_playerDatas.Any(e => e.EntityController == newPlayerController)) {    // Player is already registered.
            return;
        }
        PlayerInput newPlayerInput = newPlayerController.gameObject.AddComponent<PlayerInput>();
        
        PlayerData newPlayerData = new PlayerData {
            RewiredPlayer = ReInput.players.GetPlayer(_playerDatas.Count),
            PlayerObject = newPlayerController.gameObject,
            EntityController = newPlayerController,
            PlayerInput = newPlayerInput
        };
        _playerDatas.Add(newPlayerData);
        newPlayerInput.Init(newPlayerData);
        newPlayerController.PlayerData = newPlayerData;
    }

    public List<PlayerData> LivingPlayers() {
        return _playerDatas.Where(e => e.PlayerObject != null).ToList();
    }

    public PlayerData NearestPlayer(Vector2 point) {
        var livingPlayers = LivingPlayers();
        if (livingPlayers.Count == 0) return null;
        return livingPlayers.OrderBy(e => Vector2.Distance(point, e.PlayerObject.transform.position)).FirstOrDefault();
    }
    
}
