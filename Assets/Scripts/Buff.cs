﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buff {
    
    public enum Positivity {
        Positive,
        Neutral,
        Negative
    }

    protected bool _ending;

    public virtual Positivity Valence => Positivity.Neutral;
    public virtual bool Expired => _ending;

    public virtual EntityStatState ApplyBuff(EntityStatState prevState) {
        return prevState;
    }

    public virtual void End() {
        _ending = true;
    }

}
