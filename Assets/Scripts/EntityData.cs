﻿using UnityEngine;

[CreateAssetMenu(menuName = "Entity/Entity Data")]
public class EntityData : ScriptableObject {

    public string ID;
    public float MaxHP;
    public float MoveSpeed;

}
