﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EntityController))]
public class EnemyControllerTestGunner : EnemyController {

    // Movement State
    public EnemyStateFollowPlayer FollowPlayerState;
    
    // Attack States
    public List<EnemyState> AttackStates;
    private EnemyState _activeAttackState;
    
    public void Start() {
        FollowPlayerState.Initialize(_entityController);
        foreach (EnemyState state in AttackStates) {
            state.Initialize(_entityController);
            state.RegisterForStateStart(OnAttackStateStarted);
        }
        CreateStateLoop(AttackStates);
        _activeAttackState = AttackStates[0];
        _activeAttackState.StartState();
    }

    private void OnAttackStateStarted(EnemyState state) {
        _activeAttackState = state;
    }

    private void Update() {
        FollowPlayerState.UpdateState();
        if (_activeAttackState) {
            _activeAttackState.UpdateState();
        }
    }
    
}
