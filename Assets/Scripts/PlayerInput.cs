﻿using Rewired;
using UnityEngine;

public class PlayerInput : MonoBehaviour {
    
    private PlayerData _playerData;
    private Player Player => _playerData.RewiredPlayer;
    private EntityController Controller => _playerData.EntityController;
    
    public void Init(PlayerData playerData) {
        _playerData = playerData;
    }

    void Update() {
        Vector2 moveVec = new Vector2(Player.GetAxis("MoveX"), Player.GetAxis("MoveY"));
        Vector2 aimVec = new Vector2(Player.GetAxis("AimX"), Player.GetAxis("AimY"));
        Controller.Move(moveVec);
        Controller.Aim(aimVec);
    }
    
}
