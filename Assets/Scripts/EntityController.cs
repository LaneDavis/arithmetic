﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EntityController : MonoBehaviour {

    [HideInInspector] public PlayerData PlayerData;

    public GunController Gun;

    private Transform _transform;
    private Rigidbody2D _rb;

    public EntityData EntityData;
    private float _curHP;
    
    public GameObject FXTarget;

    public GameObject DeathSplat;
    
    private List<Vector2> _moveVectors = new List<Vector2>();

    // The entities stats before buffs are applied.
    private EntityStatState _baseStats;
    
    private void Awake() {
        _transform = transform;
        _rb = GetComponent<Rigidbody2D>();
        _curHP = EntityData.MaxHP;
    }

    public void Move(Vector2 vec) {
        if (PlayerData != null) {    // For players, process movement instantaneously.
            _rb.velocity = vec * EntityData.MoveSpeed;
            return;
        }
        _moveVectors.Add(vec);       // For enemies, sum movement vectors at end of update.
    }

    private void Update() {
        Vector2 moveSum = new Vector2(_moveVectors.Sum(e => e.x), _moveVectors.Sum(e => e.y));
        if (moveSum.magnitude > 1f) {
            moveSum = moveSum.normalized;
        }
        _moveVectors.Clear();
        _rb.velocity = moveSum * EntityData.MoveSpeed;
    }

    public void Aim(Vector2 vec) {
        if (vec.magnitude > 0.1f) {
            Gun.Shoot(vec);
        }
    }

    public void ReceiveHit(BulletController bullet) {
        var bulletTransform = bullet.transform;
        Vector2 diff = transform.position - bulletTransform.position;
        Vector2 dir = bulletTransform.right;
        Vector2 inputVec = (diff + dir) / 2f;
        StandardFX.DoEffect("Receive Hit", FXTarget, new FXArgs{InputVector = inputVec});
        _curHP -= 1f;
        if (_curHP <= 0f) {
            Die();
        }
    }

    private void Die() {
        if (PlayerData != null) {
            RumbleManager.Instance.UnregisterPlayer(PlayerData);
        }
        GameObject deathSplat = PoolManager.Instance.GetCopy(DeathSplat);
        deathSplat.transform.position = transform.position;
        deathSplat.SetActive(true);
        Destroy(gameObject);
    }

}
