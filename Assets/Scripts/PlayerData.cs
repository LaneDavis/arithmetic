﻿using Rewired;
using UnityEngine;

public class PlayerData {

    public Player RewiredPlayer;
    public GameObject PlayerObject;
    public EntityController EntityController;
    public PlayerInput PlayerInput;

}
