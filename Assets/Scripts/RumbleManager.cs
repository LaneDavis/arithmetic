﻿using System.Collections.Generic;
using UnityEngine;

public class RumbleManager : MonoBehaviour {

    public static RumbleManager Instance;
    
    public AnimationCurve RumbleStrengthByDistance;
    public float ControllerSideOffsetDistance;
    public float UpdateHz;

    private class RumblePoint {
        public Vector2 Position;
        public float Strength;
        public float TimeOfExpiration;
    }
    
    private List<RumblePoint> _fireAndForgetRumbles = new List<RumblePoint>();
    private Dictionary<object, RumblePoint> _lastingRumbles = new Dictionary<object, RumblePoint>();
    private List<PlayerData> _playerDatas = new List<PlayerData>();
    private float _timer;

    private void Awake() {
        Instance = this;
    }
    
    public void AddFireAndForgetRumble (Vector2 position, float strength, float duration) {
        _fireAndForgetRumbles.Add(new RumblePoint{Position = position, Strength = strength, TimeOfExpiration = Time.time + duration});
        _timer = 1f;
    }

    public void SetLastingRumble(object rumbleProducer, Vector2 position, float strength) {
        if (_lastingRumbles.ContainsKey(rumbleProducer)) {
            _lastingRumbles[rumbleProducer].Position = position;
            _lastingRumbles[rumbleProducer].Strength = strength;
            _timer = 1f;
        }
        else {
            _lastingRumbles.Add(rumbleProducer, new RumblePoint {Position = position, Strength = strength});
        }
    }

    public void RemoveLastingRumble(object rumbleProducer) {
        if (_lastingRumbles.ContainsKey(rumbleProducer)) {
            _lastingRumbles.Remove(rumbleProducer);
        }
    }

    public void RegisterPlayer(PlayerData player) {
        _playerDatas.AddIfUnique(player);
    }

    public void UnregisterPlayer(PlayerData player) {
        _playerDatas.Remove(player);
    }

    private void Update() {
        for (int i = _fireAndForgetRumbles.Count - 1; i >= 0; i--) {
            if (Time.time > _fireAndForgetRumbles[i].TimeOfExpiration) {
                _fireAndForgetRumbles.RemoveAt(i);
            }
        }
        
        _timer += Time.unscaledTime;
        if (_timer > 1f / UpdateHz) {
            foreach (PlayerData pd in _playerDatas) {
                Vector2 playerPos = pd.PlayerObject.transform.position;
                Vector2 left = playerPos + Vector2.right * -ControllerSideOffsetDistance;
                Vector2 right = playerPos + Vector2.right * ControllerSideOffsetDistance;
                float leftRumble = 0f;
                float rightRumble = 0f;
                foreach (RumblePoint rp in _fireAndForgetRumbles) {
                    float leftStr = rp.Strength * RumbleStrengthByDistance.Evaluate(Vector2.Distance(left, rp.Position));
                    float rightStr = rp.Strength * RumbleStrengthByDistance.Evaluate(Vector2.Distance(right, rp.Position));
                    leftRumble = Mathf.Max(leftRumble, leftStr);
                    rightRumble = Mathf.Max(rightRumble, rightStr);
                }
                foreach (RumblePoint rp in _lastingRumbles.Values) {
                    float leftStr = rp.Strength * RumbleStrengthByDistance.Evaluate(Vector2.Distance(left, rp.Position));
                    float rightStr = rp.Strength * RumbleStrengthByDistance.Evaluate(Vector2.Distance(right, rp.Position));
                    leftRumble = Mathf.Max(leftRumble, leftStr);
                    rightRumble = Mathf.Max(rightRumble, rightStr);
                }
                pd.RewiredPlayer.SetVibration(0, leftRumble);
                pd.RewiredPlayer.SetVibration(1, rightRumble);
            }
        }
    }

}
