﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BulletTrail : MonoBehaviour, IPoolableObject {

    private List<ParticleSystem> _particleSystems;
    private GameObject _followedBullet;
    private float _waitAfterBulletDeath;
    private Transform _transform;
    private bool _deathScheduled;
    private float _timeOfDeath;

    public void Init(BulletController bulletController) {
        _followedBullet = bulletController.gameObject;
        bulletController.OnBulletDeath += OnBulletDeath;
        bulletController.OnSortShift += OnSortShift;
        _particleSystems = GetComponentsInChildren<ParticleSystem>().ToList();
        if (_particleSystems.Count > 0) {
            _waitAfterBulletDeath = _particleSystems.Max(e => e.main.startLifetime.constantMax);
        }
        foreach (ParticleSystem ps in _particleSystems) {
            ps.Play();
        }
        _transform = transform;
    }

    private void OnSortShift(BulletController bulletController) {
        SetParticleSortOrder(bulletController.SpriteRenderer.sortingOrder);
    }

    private void SetParticleSortOrder(int order) {
        foreach (ParticleSystem ps in _particleSystems) {
            ps.GetComponent<Renderer>().sortingOrder = order;
        }
    }

    private void OnBulletDeath(BulletController bulletController) {
        foreach (ParticleSystem ps in _particleSystems) {
            ps.Stop();
        }
        _deathScheduled = true;
        _timeOfDeath = Time.time + _waitAfterBulletDeath;
    }

    private void Update() {
        if (!_deathScheduled) {
            _transform.position = _followedBullet.transform.position;
        }
        else {
            if (Time.time > _timeOfDeath) {
                ResetForPool();
                ReleaseToPool();
            }
        }
    }

    public GameObject GameObject() {
        return gameObject;
    }

    public void ResetForPool() {
        SetParticleSortOrder(0);
        _deathScheduled = false;
    }

    public void ReleaseToPool() {
        PoolManager.Instance.ReturnCopy(this);
    }
}
