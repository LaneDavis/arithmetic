﻿using UnityEngine;

public class BulletController : MonoBehaviour, IPoolableObject {

    public LayerMask TargetMask;
    public float Speed = 10f;
    public SpriteRenderer SpriteRenderer;
    private const float RaySizeIncrement = 0.1f;
    private Transform _myTransform;

    private float _age;
    private bool _hasDoneSortShift;
    private const float AgeOfSortShift = 0.15f;
    public static int LastSortOrder;

    public GameObject DeathSplatPrefab;

    public delegate void BulletEvent(BulletController bulletController);
    public event BulletEvent OnSortShift;
    public event BulletEvent OnBulletDeath;
    
    private void OnEnable() {
        _myTransform = transform;
    }
    
    private void Update() {
        UpdateAge();
        RaycastCollision();
        Move();
        transform.Translate(Vector2.right * (Time.deltaTime * Speed));
    }

    private void UpdateAge() {
        _age += Time.deltaTime;
        if (!_hasDoneSortShift && _age > AgeOfSortShift) {
            if (LastSortOrder < SortOrder.BulletsMin || LastSortOrder >= SortOrder.BulletsMax) {
                LastSortOrder = SortOrder.BulletsMin;
            }
            else {
                LastSortOrder++;
            }
            SpriteRenderer.sortingOrder = LastSortOrder;
            OnSortShift?.Invoke(this);
            _hasDoneSortShift = true;
        }
    }
    
    private void RaycastCollision () {

        int numberOfCasts = 1 + (int) (transform.localScale.y / RaySizeIncrement);
        for (int i = 0; i < numberOfCasts; i++) {
            var yDist = 0.5F * transform.localScale.y - i * RaySizeIncrement;
            
            Vector2 castPoint = _myTransform.position + _myTransform.up * yDist;
            var xDist = Mathf.Sqrt(Mathf.Pow(_myTransform.localScale.y * 0.5F, 2F) - Mathf.Pow(yDist, 2F));

            var lineCastStart = castPoint - (Vector2) transform.right * xDist;
            var lineCastEnd = castPoint + (Vector2) _myTransform.right * (xDist + Speed * Time.deltaTime);

            var hit = Physics2D.Linecast(lineCastStart, lineCastEnd, TargetMask);
            
            if (hit.collider != null) {
                if (Hit(hit.collider)) {
                    break;
                }
            } 
        }
		
    }

    private bool Hit(Collider2D hit) {
        bool delete = false;
        
        if (hit.gameObject.layer == 8) {    // Hits a wall.
            delete = true;
        }

        if (hit.gameObject.layer == 11) {    // Hits an enemy.
            EntityController controller = hit.GetComponent<EntityController>();
            if (controller != null) {
                controller.ReceiveHit(this);
                delete = true;
            }
        }
        
        if (delete) {
            DeleteBullet();
            return true;
        }

        return false;
    }

    private void Move() {
        _myTransform.Translate(Vector2.right * (Time.deltaTime * Speed));
    }

    private void DeleteBullet() {
        GameObject splat = PoolManager.Instance.GetCopy(DeathSplatPrefab);
        splat.transform.position = transform.position;
        splat.SetActive(true);
        ResetForPool();
        ReleaseToPool();
    }

    public GameObject GameObject() {
        return gameObject;
    }

    public void ResetForPool() {
        SpriteRenderer.sortingOrder = 0;
        OnBulletDeath?.Invoke(this);
        OnBulletDeath = null;
        OnSortShift = null;
    }

    public void ReleaseToPool() {
        PoolManager.Instance.ReturnCopy(this);
    }
}
