﻿public class FxCamshake : FX {

    public float Amplitude;
    
    public override void Trigger(FXArgs args) {
        CameraController.Instance.DoShake(args.Amplitude * Amplitude);
    }
    
}