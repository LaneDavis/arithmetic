﻿using UnityEngine;

[RequireComponent(typeof(EntityController))]
public class PlayerEntityRegister : MonoBehaviour {

    private void Start() {
        EntityController controller = GetComponent<EntityController>();
        PlayerManager.Instance.RegisterPlayerEntity(GetComponent<EntityController>());
        RumbleManager.Instance.RegisterPlayer(controller.PlayerData);
        Destroy(this);
    }
    
}
